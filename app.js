var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cloudinary = require('cloudinary');
var multer = require('multer');
var method_override = require('method-override');
var favicon = require('serve-favicon');

var Schema = mongoose.Schema;

var upload = multer({dest: './uploads'});
var app_password = '1234';

cloudinary.config({
  cloud_name: 'heberqc',
  api_key: '836596163463425',
  api_secret: 'LdKWegGOHUg4xX1PdFwfJp5Fb3E'
});

var app = express();

var _db_url = process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://localhost/';
var _db_name = process.env.OPENSHIFT_APP_NAME || 'primera';
mongoose.connect(_db_url + _db_name);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(method_override("_method"));

// Definir el schema de nuestro productos
var productSchemaJSON = {
  title: String,
  description: String,
  imageURL: String,
  pricing: Number
};

var productSchema = new Schema(productSchemaJSON);
productSchema.virtual('image.url').get(function () {
  if (!this.imageURL || this.imageURL === '' || this.imageURL === 'data.png') {
    return 'default.gif'
  }
  return this.imageURL;
});

var Product = mongoose.model('Product', productSchema);

app.set('view engine', 'jade');

app.use(express.static('public'));
app.use(favicon(__dirname + '/public/favicon.ico'));

app.get('/', function(req, res) {
  res.render('index');
});

app.get('/menu', function (req, res) {
  Product.find(function (err, documento) {
    if (err) {
      console.log(err);
    }
    res.render('menu/index', { products: documento });
  })
});

app.post("/menu", upload.single('image_avatar'), function (req, res) {
  // console.log(req.body);
  if (req.body.password === app_password) {
    var data = {
      title: req.body.title,
      description: req.body.description,
      pricing: req.body.pricing
    };

    var product = new Product(data);

    if (!!req.file) {
      cloudinary.uploader.upload(req.file.path, function(result) {
        product.imageURL = result.url;
        product.save(function (err) {
          console.log(product);
          res.rendirect('/menu');
        });
      });
    } else {
      product.save(function (err) {
        console.log(product);
        res.render('index');
      });
    }
  } else {
    res.render('menu/new');
  }

});

app.get('/menu/new', function (req, res) {
  res.render('menu/new');
});

app.get('/admin', function (req, res) {
  res.render('admin/form');
});

app.post('/admin', function (req, res) {
  if (req.body.password === app_password) {
    Product.find(function (err, documento) {
      if (err) {
        console.log(err);
      }
      res.render('admin/index', { products: documento });
    });
  } else {
    res.redirect("/");
  }
});

app.get('/menu/edit/:id', function (req, res) {
  var id_producto = req.params.id;
  Product.findOne({_id: id_producto}, function (err, producto) {
    console.log(producto);
    res.render('menu/edit', { product: producto });
  });
});

app.put("/menu/:id", upload.single('image_avatar'), function (req, res) {
  if (req.body.password === app_password) {
    var data = {
      title: req.body.title,
      description: req.body.description,
      pricing: req.body.pricing
    };
    if (!!req.file && req.file.hasOwnProperty('originalname')) {
      cloudinary.uploader.upload(req.file.path, function(result) {
        data.imageURL = result.url;
        Product.update({_id: req.params.id}, data, function (product) {
          res.redirect("/menu");
        });
      });
    } else {
      Product.update({_id: req.params.id}, data, function (product) {
        res.redirect("/menu");
      });
    }
  } else {
    res.redirect("/");
  }
});

app.get('/menu/:id/delete', function (req, res) {
  var id = req.params.id;
  Product.findOne({'_id': id}, function (err, producto) {
    res.render('menu/delete', {producto: producto});
  });
});

app.delete('/menu/:id', function (req, res) {
  var id = req.params.id;
  if (req.body.password === app_password) {
    Product.remove({"_id": id}, function (err) {
      if (err) {
        console.log(err);
      }
      res.redirect('/menu');
    });
  } else {
    res.redirect('/menu')
  }
});

app.get('/contacto', function (req, res) {
  res.render('contacto');
});

var port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

app.listen(port, ip);
